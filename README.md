# Sisperdotze

A 6x12 font created in the "Fonts.txt" workshop at the Free Culture Forum 2014,
facilitated by Manufactura Independente. It was designed and built in 4 hours.

Read more about the workshop at the [Type:Bits site](https://typebits.gitlab.io).

![Font preview](https://gitlab.com/typebits/font-sisperdotze/-/jobs/artifacts/master/raw/Sisperdotze-Regular.png?job=build-font)
  
[See it in action](https://typebits.gitlab.io/font-sisperdotze/)

Download formats:

* [TrueType (.ttf)](https://gitlab.com/typebits/font-sisperdotze/-/jobs/artifacts/master/raw/Sisperdotze-Regular.ttf?job=build-font)
* [OpenType (.otf)](https://gitlab.com/typebits/font-sisperdotze/-/jobs/artifacts/master/raw/Sisperdotze-Regular.otf?job=build-font)
* [FontForge (.sfd)](https://gitlab.com/typebits/font-sisperdotze/-/jobs/artifacts/master/raw/Sisperdotze-Regular.sfd?job=build-font)

## Authors

* Kira Riera Contijoch
* María Florencia Fernández
* Maria Luisa Jimenez, [@marylu07](https://twitter.com/marylu07)
* Irene Farré Márquez, [@irenefarrem](https://twitter.com/irenefarrem)
* Óscar Pereira
* Dario Trapasso, [@dariotrapasso](https://twitter.com/dariotrapasso)

## License

This font is made available under the [Open Font License](https://scripts.sil.org/OFL).
